package uga.tpspark.flickr

import java.net.URLDecoder
import org.apache.spark.rdd.RDD
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import uga.tpspark.flickr.Picture

object FlickrExercise {
  def main(args: Array[String]): Unit = {
    // executing Spark on your machine, using 6 threads
    val conf = new SparkConf().setMaster("local[6]").setAppName("Flickr batch processing")
    val sc = new SparkContext(conf)

    // Control our logLevel. we can pass the desired log level as a string.
    // Valid log levels include: ALL, DEBUG, ERROR, FATAL, INFO, OFF, TRACE, WARN
    sc.setLogLevel("WARN")
    try {
      val originalFlickrMetaData: RDD[String] = sc.textFile("flickrSample.txt")
      //All explanations of this code is inside the readme.md at the root of this project.
      /******Question 1******/
      originalFlickrMetaData.take(5)foreach(println)
      println(s"Number of elements: ${originalFlickrMetaData.count()}")

      /*****Question 2*****/
      val convertToPicture = originalFlickrMetaData.map(x => new Picture(x.split("\\t")))
        .filter(x=> x.hasTags && x.hasValidCountry)
      convertToPicture.take(5).foreach(println)

      /*****Question 3*****/
      val groupImagesByCountry: RDD[(Country, Iterable[Picture])] = convertToPicture.groupBy(x=> x.c)
      println("*****Question 3*****")
      groupImagesByCountry.take(1).foreach(println)

      /*****Question 4*****/
      val groupTagsByCountry: RDD[(Country, Iterable[String])] = groupImagesByCountry.map(x=>(x._1, x._2.flatMap(x=> x.userTags)))
      println("*****Question 4*****")
      groupTagsByCountry.take(1).foreach(println)

      /*****Question 5*****/
      println("*****Question 5*****")
      val groupTagsByCountryWithFreq:RDD[(Country, Map[String, Int])] = groupTagsByCountry.map(x => (x._1, x._2.groupBy(identity).map(t => (t._1, t._2.size))))
      groupTagsByCountryWithFreq.take(1).foreach(println)

      /****Question 6*****/
      println("*****Question 6*****")
      val otherMethod:RDD[(Country, Map[String, Int])] = convertToPicture.flatMap(x=> x.userTags.map(y => ((x.c, y), 1)))
        .reduceByKey(_+_).map(x => (x._1._1, (x._1._2, x._2 ))).groupByKey().map(x =>(x._1, x._2.toMap))
      otherMethod.take(1).foreach(println)

    } catch {
      case e: Exception => throw e
    } finally {
      sc.stop()
    }
    println("done")
  }
}