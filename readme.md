##TP Data Management: analysis of flickr tags using spark

**Question 1**

`count()` is used in order to count the number of elements of the RDD.

**Question 2**

`originalFlickrMetaData` RDD is used to create a new RDD by mapping all its line by the corresponding Picture object. `filter` function and `hasTags` , `hasValidCountry` Picture functions were then used to keep only interesting pictures having a valid country and tags.

**Question 3**

After grouping these Pictures by country the new obtained RDD is of type`RDD[(Country, Iterable[Picture])]`. The new RDD is an RDD of tuples country and all its associated Pictures.

**Question 4**

The previous RDD has been used to create a new RDD containing pairs in which the first element is a country, and the second element is the list of tags used on pictures taken in this country. To do so we use `flatmap` to retrieves tags associated with each Picture.

**Question 5**

Given the RDD `groupTagsByCountry` which has for key the country and for value the list of tags associated with that country (there could be redundancy of tags in the list), we want to have a new RDD (country, list(tag, freq)). 
We will keep the key in the new RDD, but for the value we use `groupBy(x=>x)` or `groupBy(identity)` which will create in the latter list, a new key value pairs made up of (tag, repetition_of_tags). Last thing, we change repetition_of_tags by the number of elements it contains using the method `size()`.

**Question 6**

Here we want to delay as long as possible the use of `groupBy` for it will limit the parallelism to the number of countries that is quite small in comparisons to the number of tags.
To achieve that, we start off we the RDD `convertToPicture` of the question 2.
Using `convertToPicture`, 

- we define a new RDD using the couple (country, tag) as key and 1 as value. From then we use almost the same computation as in Worldcount to get the number of tag per key (country, tag). 

- And at the end we use a groupBy to get the list of (tag, frequence) per country using groupBy. 

Our sequence of operation is as follow: convertToPicture = RDD(pictures) ==> RDD((country, tag), 1) ==> RDD ((country, tag), sum) ==> groupBy to get RDD(country, list(tag, sum))
